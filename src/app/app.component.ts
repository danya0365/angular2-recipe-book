import { Component } from '@angular/core';
import { LogService } from './services/';

@Component({
  selector: 'rb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    constructor(private logService: LogService){
        this.logService.writeToLog('init');
    }


}
