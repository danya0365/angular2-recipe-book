import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyAccountComponent, MyAccountRoutingModule, MyAccountHomeComponent, MyAccountEditComponent } from './';

@NgModule({
  imports: [
    CommonModule,
    MyAccountRoutingModule
  ],
  declarations: [MyAccountComponent, MyAccountHomeComponent, MyAccountEditComponent]
})
export class MyAccountModule { }
