import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyAccountComponent } from './my-account.component';
import { MyAccountHomeComponent } from './my-account-home.component';
import { MyAccountEditComponent } from './my-account-edit.component';
import { AuthGuardService } from './../services/';

const routes: Routes = [
    {
        path: '',
        component: MyAccountComponent,
        canActivate: [AuthGuardService],
        children: [
            {
                path: '',
                component: MyAccountHomeComponent
            },
            {
                path: 'edit',
                component: MyAccountEditComponent
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MyAccountRoutingModule { }
