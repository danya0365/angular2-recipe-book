import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RecipeBookRoutingModule } from './app-routing.module';
import { HomeRoutingModule } from './home/home-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header.component';
import { DropdownDirective } from './dropdown.directive';
import { LogService, RecipeService, ShoppingListService, AuthService, AuthGuardService } from './services/';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DropdownDirective,
    HomeComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RecipeBookRoutingModule,
    HomeRoutingModule
  ],
  providers: [LogService, RecipeService, ShoppingListService, AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
