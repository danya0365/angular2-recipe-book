import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipesComponent, RecipesRoutingModule } from './';
import { RecipeListComponent, RecipeItemComponent } from './recipe-list/';
import { RecipeDetailComponent } from './recipe-detail/';
import { RecipeEditComponent } from './recipe-edit/';

import { ShoppingModule } from '../shopping/shopping.module';

@NgModule({
  imports: [
    CommonModule,
    RecipesRoutingModule,
    ShoppingModule
  ],
  declarations: [RecipesComponent, RecipeListComponent, RecipeDetailComponent, RecipeEditComponent, RecipeItemComponent]
})
export class RecipesModule { }
