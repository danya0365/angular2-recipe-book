import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Recipe } from '../recipe';
import { RecipeService, ShoppingListService } from '../../services/';

@Component({
    selector: 'rb-recipe-detail',
    templateUrl: './recipe-detail.component.html',
    styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit, OnDestroy {
    selectedRecipe: Recipe = null;
    private selectedRecipeSubscription;

    constructor(
        private recipeService: RecipeService,
        private shoppingListService: ShoppingListService,
        private router: Router,
        private activatedRoute: ActivatedRoute) {
        this.selectedRecipeSubscription = recipeService.recipeChange.subscribe((value) => {
            this.selectedRecipe = value;
        });
    }

    ngOnDestroy() {

    }

    ngOnInit() {

        //https://angular.io/docs/ts/latest/guide/router.html#!#optional-route-parameters
        this.activatedRoute.params.forEach((params: Params) => {
            console.log('params', params)
           //let id = +params['id']; // (+) converts string 'id' to a number
           let id = params['id']; // (+) converts string 'id' to a number
           this.selectedRecipe = this.recipeService.getRecipeWithid(id);
         });

         this.activatedRoute.queryParams.forEach((params: Params) => {
            console.log('queryParams', params)
          });

        if ( this.selectedRecipe == null )
            this.selectedRecipe = this.recipeService.getSelectedRecipe();

        if ( this.selectedRecipe == null )
            this.router.navigate(['/recipes']);
    }

    onAddToShoppingList(){

        if ( !this.selectedRecipe ) return;
        this.shoppingListService.addIngredients(this.selectedRecipe.ingredients);
    }

    onEdit(): void {
        this.router.navigate(['recipes', this.selectedRecipe.id, 'edit']);
    }

}
