import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Recipe } from '../recipe';
import { RecipeService } from '../../services/';

@Component({
    selector: 'rb-recipe-list',
    templateUrl: './recipe-list.component.html',
    styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
    recipes: Recipe[] = [];

    constructor(
        private recipeService: RecipeService,
        private router: Router,
        private activatedRoute: ActivatedRoute) { }

    ngOnInit() {

        this.recipes = this.recipeService.getRecipes();
        if ( this.recipes.length == 0 )
            this.recipes = this.recipeService.generateMockupData().getRecipes();

    }

    setSelectedRecipe(recipe: Recipe) {
        //console.log(recipe);
        this.recipeService.setSelectedRecipe(recipe);

        let navigationExtras = {
          queryParams: {'test1':'1', 'test2':'2'},
          fragment: 'anchor'
        };

        this.router.navigate(['/recipes', recipe.id], navigationExtras);
    }
}
