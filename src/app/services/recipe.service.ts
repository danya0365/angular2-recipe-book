import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Recipe } from '../recipes/recipe';
import { Ingredient } from '../shared/';

@Injectable()
export class RecipeService {
    private baifernImg: string[] = ['https://image.dek-d.com/27/0407/2245/118916159', 'https://image.dek-d.com/27/0407/2245/118916301', 'https://image.dek-d.com/27/0407/2245/118916269', 'https://image.dek-d.com/27/0407/2245/118914863', 'https://image.dek-d.com/27/0407/2245/118916271', 'https://image.dek-d.com/27/0407/2245/118916182', 'https://image.dek-d.com/27/0407/2245/118914855', 'https://image.dek-d.com/27/0407/2245/118916306', 'https://image.dek-d.com/27/0407/2245/118916185', 'https://image.dek-d.com/27/0407/2245/118916262', 'https://image.dek-d.com/27/0407/2245/118916232', 'https://image.dek-d.com/27/0407/2245/118916179', 'https://image.dek-d.com/27/0407/2245/118914878', 'https://image.dek-d.com/27/0407/2245/118914898', 'https://image.dek-d.com/27/0407/2245/118916267', 'https://image.dek-d.com/27/0407/2245/118916354', 'https://image.dek-d.com/27/0407/2245/118914920', 'https://image.dek-d.com/27/0407/2245/118916166', 'https://image.dek-d.com/27/0407/2245/118916273', 'https://image.dek-d.com/27/0407/2245/118916310', 'https://image.dek-d.com/27/0407/2245/118916297', 'https://image.dek-d.com/27/0407/2245/118916364', 'https://image.dek-d.com/27/0407/2245/118914913', 'https://image.dek-d.com/27/0407/2245/118916081', 'https://image.dek-d.com/27/0407/2245/118914929', 'https://image.dek-d.com/27/0407/2245/118914884', 'https://image.dek-d.com/27/0407/2245/118916199', 'https://image.dek-d.com/27/0407/2245/118914897', 'https://image.dek-d.com/27/0407/2245/118916221', 'https://image.dek-d.com/27/0407/2245/118916119', 'https://image.dek-d.com/27/0407/2245/118916298', 'https://image.dek-d.com/27/0407/2245/118916264', 'https://image.dek-d.com/27/0407/2245/118914919', 'https://image.dek-d.com/27/0407/2245/118914911', 'https://image.dek-d.com/27/0407/2245/118916305', 'https://image.dek-d.com/27/0407/2245/118916376', 'https://image.dek-d.com/27/0407/2245/118916189', 'https://image.dek-d.com/27/0407/2245/118916115', 'https://image.dek-d.com/27/0407/2245/118916299', 'https://image.dek-d.com/27/0407/2245/118916299', 'https://image.dek-d.com/27/0407/2245/118916351', 'https://image.dek-d.com/27/0407/2245/118916095', 'https://image.dek-d.com/27/0407/2245/118916226', 'https://image.dek-d.com/27/0407/2245/118916161', 'https://image.dek-d.com/27/0407/2245/118916361', 'https://image.dek-d.com/27/0407/2245/118916142', 'https://image.dek-d.com/27/0407/2245/118916373', 'https://image.dek-d.com/27/0407/2245/118916128', 'https://image.dek-d.com/27/0407/2245/118914874', 'https://image.dek-d.com/27/0407/2245/118916363', 'https://image.dek-d.com/27/0407/2245/118916174', 'https://image.dek-d.com/27/0407/2245/118916366', 'https://image.dek-d.com/27/0407/2245/118914902', 'https://image.dek-d.com/27/0407/2245/118916165', 'https://image.dek-d.com/27/0407/2245/118916090', 'https://image.dek-d.com/27/0407/2245/118916242', 'https://image.dek-d.com/27/0407/2245/118916152', 'https://image.dek-d.com/27/0407/2245/118916065', 'https://image.dek-d.com/27/0407/2245/118916303', 'https://image.dek-d.com/27/0407/2245/118916092', 'https://image.dek-d.com/27/0407/2245/118916357', 'https://image.dek-d.com/27/0407/2245/118916302', 'https://image.dek-d.com/27/0407/2245/118916369', 'https://image.dek-d.com/27/0407/2245/118916086', 'https://image.dek-d.com/27/0407/2245/118916278', 'https://image.dek-d.com/27/0407/2245/118916072', 'https://image.dek-d.com/27/0407/2245/118916370', 'https://image.dek-d.com/27/0407/2245/118916107', 'https://image.dek-d.com/27/0407/2245/118914860', 'https://image.dek-d.com/27/0407/2245/118916121', 'https://image.dek-d.com/27/0407/2245/118916188', 'https://image.dek-d.com/27/0407/2245/118916190', 'https://image.dek-d.com/27/0407/2245/118916296', 'https://image.dek-d.com/27/0407/2245/118916134', 'https://image.dek-d.com/27/0407/2245/118914881', 'https://image.dek-d.com/27/0407/2245/118916071', 'https://image.dek-d.com/27/0407/2245/118916224', 'https://image.dek-d.com/27/0407/2245/118916122'];

    private recipes: Recipe[] = [];
    private selectedRecipe: Recipe = null;
    public recipeChange: Subject<Recipe> = new Subject<Recipe>();
    constructor() { }

    generateMockupData() {
        for ( var i = 0; i < this.baifernImg.length; i++ ){
        //for ( var i = 0; i < 5; i++ ){

            var ingredients = [
                new Ingredient('ส่วนประกอบ ' + i + '+1', 1),
                new Ingredient('ส่วนประกอบ ' + i + '+2', 1),
                new Ingredient('ส่วนประกอบ ' + i + '+3', 1),
            ]
            this.recipes.push(  new Recipe(Math.random().toString(36).substr(2, 9), 'ใบเฟิร์น ' + (i+1), 'ใบเฟิร์นฟรุ่งฟริ้ง ' + (i+1), this.baifernImg[i], ingredients) )
        }
        return this;
    }

    getRecipes(){
        return this.recipes;
    }

    setSelectedRecipe(recipe: Recipe): void {
        this.selectedRecipe = recipe;
        this.recipeChange.next(this.selectedRecipe);
    }

    getSelectedRecipe(): Recipe {
        return this.selectedRecipe;
    }

    getRecipeWithid(recipeId: string): Recipe {
        var returnRecipe: Recipe = null;
        this.recipes.forEach((item: Recipe)=> {
            if ( item.id == recipeId ) {
                returnRecipe = item;
            }
        })
        return returnRecipe;
    }

}
