import { Injectable } from '@angular/core';
import { Ingredient } from '../shared/';

@Injectable()
export class ShoppingListService {

    private ingredients: Ingredient[] = [];
    constructor() { }

    getIngredients() {
        return this.ingredients;
    }

    addIngredient(ingredient: Ingredient){
        this.ingredients.push(ingredient);
    }

    addIngredients(ingredients: Ingredient[]){
        /*
        ingredients.forEach((ingredient: Ingredient) => {
            this.ingredients.push(ingredient);
        })
        */
        Array.prototype.push.apply(this.ingredients, ingredients);
    }

    editIngredient(oldIngredient: Ingredient, newIngredient: Ingredient){
        this.ingredients[this.ingredients.indexOf(oldIngredient)] = newIngredient;
    }

    deleteIngredient(ingredient: Ingredient){
        this.ingredients.splice(this.ingredients.indexOf(ingredient),1);
    }
}
