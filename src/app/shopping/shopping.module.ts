import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ShoppingRoutingModule, ShoppingComponent, ShoppingListComponent, ShoppingListAddComponent } from './';

@NgModule({
  imports: [
    CommonModule,
    ShoppingRoutingModule,
    FormsModule,
  ],
  declarations: [ShoppingComponent, ShoppingListComponent, ShoppingListAddComponent],
  exports: [ShoppingListComponent, ShoppingListAddComponent]
})
export class ShoppingModule { }
