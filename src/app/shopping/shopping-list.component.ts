import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/';
import { ShoppingListService } from '../services/';

@Component({
    selector: 'rb-shopping-list',
    templateUrl: './shopping-list.component.html',
    styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

    ingredients: Ingredient[] = [];
    selectedItem: Ingredient = null;
    constructor(private shoppingListService: ShoppingListService) { }

    ngOnInit() {
        this.ingredients = this.shoppingListService.getIngredients();
    }

    onSelectItem(ingredient: Ingredient): void {
        this.selectedItem = ingredient;
    }

    onCleared(): void {
        this.selectedItem = null;
    }

}
