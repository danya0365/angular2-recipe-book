import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Ingredient } from '../shared/ingredient';
import { ShoppingListService } from '../services/shopping-list.service';

@Component({
    selector: 'rb-shopping-list-add',
    templateUrl: './shopping-list-add.component.html',
    styleUrls: ['./shopping-list-add.component.css']
})
export class ShoppingListAddComponent implements OnInit, OnChanges {

    @Input()
    ingredient: Ingredient;

    @Output() cleared = new EventEmitter();

    isAdd = true;
    constructor(private shoppingListService: ShoppingListService) { }

    ngOnInit() {
    }

    //Input changed
    ngOnChanges(changes) {
        if ( changes.ingredient.currentValue === null ) {
            this.isAdd = true
            this.ingredient = {name: null, amount: null};
        } else {
            this.isAdd = false;
        }
    }

    onDeleteClick(): void {
        console.info('onDeleteClick')
        this.shoppingListService.deleteIngredient(this.ingredient);
        this.onClearClick();
    }
    onClearClick(): void {
        console.info('onClearClick')
        this.isAdd = true;
        this.cleared.emit(null);
    }

    onSubmit(ingredient: Ingredient): void {
        console.info('onSubmit')
        const newIngredient = new Ingredient(ingredient.name, ingredient.amount);
        if ( !this.isAdd ){
            //Edit
            this.shoppingListService.editIngredient(this.ingredient, newIngredient);
            this.onClearClick();
        } else {
            this.shoppingListService.addIngredient(newIngredient);
            this.onClearClick();
        }
    }
}
