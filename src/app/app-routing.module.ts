import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './services/';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'logout', component: LoginComponent },
    {
        path: 'my-account',
        loadChildren: 'app/my-account/my-account.module#MyAccountModule',
        canLoad: [AuthGuardService]
    },
    {
        path: 'recipes',
        loadChildren: 'app/recipes/recipes.module#RecipesModule'
    },
    {
        path: 'shopping',
        loadChildren: 'app/shopping/shopping.module#ShoppingModule'
    },
    { path: '**', redirectTo: '/home' , pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class RecipeBookRoutingModule { }
